/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocios;

/**
 *
 * @author imondragon
 */
public class Calcula {

  private Double tInicial;
  private Double tFinal;
  private Double distancia;
  private Double vel;
  private Double tTotal;

  public Calcula(String tIni, String tFin, String dist) {
    tInicial = Double.parseDouble(tIni);
    tFinal = Double.parseDouble(tFin);
    distancia = Double.parseDouble(dist);
  }

  public Double getVel() {
    return vel;
  }

  public Double getTiempo() {
    return tTotal;
  }
  public void velocidad(){
    vel =  distancia / (tFinal - tInicial);
  }
  public void tiempoTotal(){
    tTotal = tFinal - tInicial;
  }
}
