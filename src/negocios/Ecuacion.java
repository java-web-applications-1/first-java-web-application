/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocios;

/**
 *
 * @author imondragon
 */
public class Ecuacion {

  private Double a, b, c;

  public Ecuacion(Double a, Double b, Double c) {
    this.a = a;
    this.b = b;
    this.c = c;
  }

  public String[] calcularSegundoGrado() {
    //Obtien los valores x1,x2 de la ecuacion
    String[] resultado = new String[2];
    double determinante = (b * b) - (4 * a * c);
    if (determinante > 0) {
      resultado[0] = Double.toString((-b + Math.sqrt(determinante)) / (2 * a));
      resultado[1] = Double.toString((-b - Math.sqrt(determinante)) / (2 * a));
      return resultado;
    }
    determinante = Math.abs(determinante);
    resultado[0] = Double.toString((-b) / (2 * a)) + "+" + (Math.sqrt(determinante) / (2 * a)) + " i";
    resultado[1] = Double.toString((-b) / (2 * a)) + "-" + (Math.sqrt(determinante) / (2 * a)) + " i";
    return resultado;
  }

}
