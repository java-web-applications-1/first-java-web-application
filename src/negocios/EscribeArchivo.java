/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocios;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import negocios.Alumno;

/**
 *
 * @author imondragon
 */
public class EscribeArchivo {

  public static void agregarAlumno(Alumno a, String path) throws IOException {
    // Se inicializan variables para trabajar con archivos
    File archivo;
    FileWriter fw = null;
    PrintWriter pw = null;
    try {
      archivo = new File(path);
      // True indica escribir al final de archivo
      fw = new FileWriter(archivo, true);
      pw = new PrintWriter(fw); // Recibe un recurso de tipo fileWriter
      pw.println(a.getNombre() + "," + a.getApellidos() + ","
              + a.getPromedio());
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (pw != null) {
          pw.close();
        }
      } catch (Exception e2) {
        e2.printStackTrace();
      }
    }
  }
}
