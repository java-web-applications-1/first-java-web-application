/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocios;

/**
 *
 * @author imondragon
 */
public class CalcularSueldo {

  private String nombre;
  private Double sueldo;
  private int diasTrab;

  public CalcularSueldo(String nombre, Double sueldo, int diasTrab) {
    this.nombre = nombre;
    this.sueldo = sueldo;
    this.diasTrab = diasTrab;
  }

  public Double getSueldo() {
    return sueldo * diasTrab;
  }

  public String getNombre() {
    return nombre;
  }
}
