package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocios.Alumno;
import negocios.LeeArchivo;

@WebServlet(name = "muestraRegistro", urlPatterns = {"/muestraRegistro"})
public class muestraRegistro extends HttpServlet {

  /**
   * Método que manda llamar el método doPost para manejar la petición que le
   * llega y manejar la respuesta que dará la cual por cuestiones de práctica no
   * se pasarán a una vista JSP. Sino que se manipulan y se regresa un html con
   * la información manipulada.
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    PrintWriter out = response.getWriter();
    try {
      ArrayList<Alumno> alumnos = new ArrayList<Alumno>();
      int cont = 0;
      String contador;
      ServletContext sc = this.getServletContext();
      String path = sc.getRealPath("/WEB-INF/Promedios.txt");
      path = path.replace('\\', '/');
      alumnos = LeeArchivo.leeAlumnos(path);
      // Resetea la variable estática
      cont = LeeArchivo.getCont();
      LeeArchivo.clearCont();
      contador = String.valueOf(cont);

      request.setAttribute("Alumnos", alumnos);
      request.setAttribute("contador", contador);
      request.getRequestDispatcher("/alumnosRegistrados.jsp")
              .forward(request, response);

    } finally {
      out.close();
    }
  }

  /**
   * Se sobre escribe dicho método para envíar al método processRequest y se le
   * pasan como parámetros el request y el response
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }
}
