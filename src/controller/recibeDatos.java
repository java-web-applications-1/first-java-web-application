/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocios.*;

@WebServlet(name = "recibeDatos", urlPatterns = {"/recibeDatos"})
public class recibeDatos extends HttpServlet {

  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    response.setContentType("text/html;charset=UTF-8");
    PrintWriter out = response.getWriter();
    try {
      String nombre = request.getParameter("nombre");
      String apellidos = request.getParameter("apellidos");
      Double promedio = Double.parseDouble(request.getParameter("prom"));

      Alumno alumno = new Alumno(nombre, apellidos, promedio);

      ServletContext sc = this.getServletContext();
      String path = sc.getRealPath("/WEB-INF/Promedios.txt");
      path = path.replace('\\', '/');

      // Guardar en archivo
      EscribeArchivo.agregarAlumno(alumno, path);
      request.setAttribute("atribAlumn", alumno);
      request.getRequestDispatcher("/muestraDatos.jsp").forward(request, response);
    } finally {
      out.close();
    }

  }

  /**
   * Se sobre escribe dicho método recibir la petición vía POST y se pasen como 
   * parámetros el request y el response
   *
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }
}
