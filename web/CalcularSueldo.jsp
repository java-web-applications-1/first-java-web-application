<%-- 
    Document   : index
    Created on : 20/01/2020, 01:29:49 PM
    Author     : imondragon
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type"
          content="text/html; charset=UTF-8">
    <title>Sueldo calculado</title>
  </head>
  <body>
    <!--Se importa la clase que hará los cálculos-->
    <%@ page import="negocios.CalcularSueldo" %>
    <%
      // Extracción de los parámetros recibidos
      String nombre = request.getParameter("nombre").trim();
      Double sueldo = Double.parseDouble(request.getParameter("sueldo").trim());
      int diasTrab = Integer.parseInt(request.getParameter("diasTra"));
      CalcularSueldo cs = new CalcularSueldo(nombre, sueldo, diasTrab);
      Double sueldoCal = cs.getSueldo();

    %>

    <h2> Resultado </h2>
    <p> Hola <%= nombre%> </p>
    <table cellspacing="3" cellpadding="3" border="1" >
      <tr>
        <td align="right"> Tu sueldo fue de: </td>
        <td> <%= sueldoCal%> Pesos </td>
      </tr>
    </table>

    <form action="index.jsp" method="post">
      <input type="submit" value="Regresar">
    </form>
  </body>
</html>