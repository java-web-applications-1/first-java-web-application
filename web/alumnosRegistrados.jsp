<%-- 
    Document   : muestraDatos
    Created on : 24/01/2020, 04:35:42 PM
    Author     : imondragon
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">   		
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <meta http-equiv="Content-Type"
          content="text/html; charset=UTF-8">
    <title>Alumnos Registrados</title>
  </head>
  <body>
    <%@ page import="negocios.Alumno, java.util.ArrayList" %>
    <%
      ArrayList<Alumno> alumnos = null;
      alumnos = (ArrayList<Alumno>) request.getAttribute("Alumnos");
      String numReg = (String) request.getAttribute("contador");
      int numRegistros = Integer.parseInt(numReg);
    %>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" 
                  data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="index.jsp">Administración de alumnos</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li>
              <a href="muestraRegistro">
                <span class="glyphicon glyphicon-th-list"></span> Ver alumnos
              </a>
            </li>
            <li>
              <a href="index.jsp">
                <span class="glyphicon glyphicon-log-in"></span> Registrar
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="container">
      <h2>Los alumnos que están registrados son: </h2>
      <div class="row">
        <div class="col-md-2 col-md-offset-10" >
          <p> Numero de registros : <%= numRegistros%></p>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-striped">
          <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Promedio</th>
          </tr>
          <%
            int contReg = 1;
            for (Alumno alumno : alumnos) {
          %>
          <tr>
            <td><%=contReg++%></td>
            <td><%=alumno.getNombre()%></td>
            <td><%=alumno.getApellidos()%></td>
            <td><%=alumno.getPromedio()%></td>
          </tr>
          <% } %>
          <% alumnos.clear();%>
        </table>
      </div>
    </div>
  </body>
</html>