<%-- 
    Document   : index
    Created on : 20/01/2020, 01:29:49 PM
    Author     : imondragon
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type"
          content="text/html; charset=UTF-8">
    <title>Pagina Destino JSP</title>
  </head>
  <body>
    <!--Se importa la clase que hará los cálculos-->
    <%@ page import="negocios.Calcula" %>
    <%
      // Extracción de los parámetros recibidos
      String nombre = request.getParameter("nombre");
      String tInicial = request.getParameter("tiempoIni");
      String tFinal = request.getParameter("tiempoFin");
      String dist = request.getParameter("distancia");
      Double vel, tiempo;
      Calcula calcula = new Calcula(tInicial, tFinal, dist);
      calcula.velocidad();
      vel = calcula.getVel();
      calcula.tiempoTotal();
      tiempo = calcula.getTiempo();
    %>

    <h2> Resultado </h2>
    <p> Hola <%= nombre%> </p>
    <table cellspacing="3" cellpadding="3" border="1" >
      <tr>
        <td align="right"> Tu tiempo total fué: </td>
        <td> <%= tiempo%> minutos </td>
      </tr>
      <tr>
        <td align="right"> Y tu velocidad: </td>
        <td> <%= vel%> metros/min</td>
      </tr>
    </table>

    <form action="index.jsp" method="post">
      <input type="submit" value="Regresar">
    </form>
  </body>
</html>