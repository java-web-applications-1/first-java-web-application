<%-- 
    Document   : muestraDatos
    Created on : 24/01/2020, 04:35:42 PM
    Author     : imondragon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">   		
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <meta http-equiv="Content-Type"
          content="text/html; charset=UTF-8">
    <title>Muestra Datos</title>
  </head>
  <body>
    <%@ page import="negocios.Alumno" %>
    <%
      Alumno alumno = (Alumno) request.getAttribute("atribAlumn");
    %>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" 
                  data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="index.jsp">Administración de alumnos</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li>
              <a href="muestraRegistro">
                <span class="glyphicon glyphicon-th-list"></span> Ver alumnos
              </a>
            </li>
            <li>
              <a href="index.jsp">
                <span class="glyphicon glyphicon-log-in"></span> Registrar
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="container">
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
        <strong>Correcto!</strong> Se registró correctamente el alumno con los siguientes datos:
      </div>

      <div class="table-responsive">
        <table class="table table-striped" >
          <tr>
            <td align="right"> Nombre: </td>
            <td> <%= alumno.getNombre()%> </td>
          </tr>
          <tr>
            <td align="right"> Apellidos: </td>
            <td> <%= alumno.getApellidos()%> </td>
          </tr>
          <tr>
            <td align="right"> Promedio: </td>
            <td> <%= alumno.getPromedio()%> </td>
          </tr>
        </table>
      </div>
    </div>
  </body>
</html>