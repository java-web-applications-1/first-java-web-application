<%-- 
    Document   : index
    Created on : 20/01/2020, 01:29:49 PM
    Author     : imondragon
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type"
          content="text/html; charset=UTF-8">
    <title>Página de Registro</title>
  </head>
  <body>
    <%@ page import="datos.EscribeArchivo, negocios.Alumno" %>
    <%
      // Obtención de los parámetros de la petición
      String nombre = request.getParameter("nombre");
      String apellidos = request.getParameter("apellidos");
      String promedio = request.getParameter("prom");

      Alumno alumno = new Alumno(nombre, apellidos,
              Double.parseDouble(promedio));
      ServletContext sc = this.getServletContext();
      String path = sc.getRealPath("/WEB-INF/Promedios.txt");
      path = path.replace('\\', '/');

      // Guardar en archivo
      EscribeArchivo.agregarAlumno(alumno, path);

    %>


    <h2>Tu registro se hizo con éxito</h2>

    <form action="index.jsp" method="post">
      <input type="submit" value="Nuevo Registro">
    </form>

  </body>
</html>