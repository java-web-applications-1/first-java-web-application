<%-- 
    Document   : index
    Created on : 20/01/2020, 01:29:49 PM
    Author     : imondragon
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type"
          content="text/html; charset=UTF-8">
    <title>Resultados</title>
  </head>
  <body>
    <!--Se importa la clase que hará los cálculos-->
    <%@ page import="negocios.Ecuacion" %>
    <%
      // Extracción de los parámetros recibidos
      Double a = Double.parseDouble(request.getParameter("a").trim());
      Double b = Double.parseDouble(request.getParameter("b").trim());
      Double c = Double.parseDouble(request.getParameter("c").trim());
      
      Ecuacion ec = new Ecuacion(a,b,c);
      String[] resultado = ec.calcularSegundoGrado();
    %>

    <h2> Resultados </h2>
    <table cellspacing="3" cellpadding="3" border="1" >
      <tr>
        <td align="right"> Primera raíz: </td>
        <td> <%= resultado[0] %> </td>
      </tr>
      <tr>
        <td align="right"> Segunda raíz: </td>
        <td> <%= resultado[1] %> </td>
      </tr>
    </table>

    <form action="index.jsp" method="post">
      <input type="submit" value="Regresar">
    </form>
  </body>
</html>