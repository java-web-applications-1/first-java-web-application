<%-- 
    Document   : index
    Created on : 20/01/2020, 01:29:49 PM
    Author     : imondragon
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">   		
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <title>Captura Datos</title>
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" 
                  data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="index.jsp">Administración de alumnos</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li>
              <a href="muestraRegistro">
                <span class="glyphicon glyphicon-th-list"></span> Ver alumnos
              </a>
            </li>
            <li>
              <a href="index.jsp">
                <span class="glyphicon glyphicon-log-in"></span> Registrar
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="container">
      <h1>Registrar alumno</h1>
      <!--El action indica a qué servlet se le enviará el formulario-->
      <form action="recibeDatos" method="post">
        <div class="form-group">
          <label for="nombre">Nombre:</label>
          <input type="text" class="form-control" id="nombre" placeholder="Nombre" 
                 name="nombre" required/>
        </div>
        <div class="form-group">
          <label for="apellidos">Apellidos:</label>
          <input type="text" class="form-control" id="apellidos" 
                 placeholder="Apellidos" name="apellidos" required/>
        </div>
        <div class="form-group">
          <label for="prom">Promedio:</label>
          <input type="text" class="form-control" id="prom" 
                 placeholder="Apellidos" name="prom" required/>
        </div>
        <div class="row" >
          <div class="col-md-6">
            <button type="resset" class="btn btn-default">Borrar</button>
          </div>
          <div class="col-md-1 col-md-offset-5">
            <button type="submit" class="btn btn-success">Registrar</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>